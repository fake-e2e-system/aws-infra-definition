provider "aws" {
  region = "${var.region}"
  version = "~> 1.24"
}

# terraform state file setup
# create an S3 bucket to store the state file in
resource "aws_s3_bucket" "remote-s3-tfstate" {
    bucket = "s3-remote-tfstate"
    force_destroy = true
    versioning {
      enabled = true
    }

    tags {
      Name = "s3-remote-tfstate"
    }
}

#create a dynamodb table for locking the state file for app config
resource "aws_dynamodb_table" "tf-app-lock" {
  name = "tf-app-lock"
  hash_key = "LockID"
  read_capacity = 2
  write_capacity = 2

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "tf-app-lock"
  }
}

#create a dynamodb table for locking the state file for app config
resource "aws_dynamodb_table" "tf-dns-lock" {
  name = "tf-dns-lock"
  hash_key = "LockID"
  read_capacity = 2
  write_capacity = 2

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "tf-dns-lock"
  }
}

#create a dynamodb table for locking the state file for app config
resource "aws_dynamodb_table" "tf-net-lock" {
  name = "tf-net-lock"
  hash_key = "LockID"
  read_capacity = 2
  write_capacity = 2

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "tf-net-lock"
  }
}

#create a dynamodb table for locking the state file for s3 static site config
resource "aws_dynamodb_table" "tf-s3-lock" {
  name = "tf-s3-lock"
  hash_key = "LockID"
  read_capacity = 2
  write_capacity = 2

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "tf-s3-lock"
  }
}
