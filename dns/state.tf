/* Back end configuration, other varibles injected via the command line

i.e. terraform init \
-backend-config "bucket=s3-remote-tfstate" \
-backend-config "dynamodb_table=tf-dns-lock" \
-backend-config "key=tf-dns/terraform.tfstate"

*/

terraform {
  backend "s3" {
    region = "eu-west-2"
    encrypt = true
  }
}
