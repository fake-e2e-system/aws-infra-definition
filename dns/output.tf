output "domain_names" {
  value = "${aws_route53_zone.zones.*.name}"
}
