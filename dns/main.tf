provider "aws" {
  region = "${var.region}"
  version = "~> 1.24"
}

/*

The DNS main.tf file create the DNS sub domains and relevent name servers in the
bigbluecow.net domain for all enviroments specifed in the dns-zones variable list.

*/

# Creates a Route53 delegation set for the bigbluecow.net domain

resource "aws_route53_delegation_set" "tdl-delegation-set" {
  reference_name = "${var.tld-zone-name}"
}
# Create the hosted zone in Route53 for zones specified in the dns-zones varible.

resource "aws_route53_zone" "zones" {
  count             = "${length(var.dns-zones)}"
  name              = "${element(var.dns-zones, count.index)}.${var.tld-zone-name}"
  delegation_set_id = "${aws_route53_delegation_set.tdl-delegation-set.id}"
  tags {
    Environment     = "${element(var.dns-zones, count.index)}"
  }
}

# populates the tld with the sub domain name servers.

resource "aws_route53_record" "name-servers" {
  count   = "${length(var.dns-zones)}"
  zone_id = "${var.tld-zone-id}"
  name    = "${element(var.dns-zones, count.index)}.${var.tld-zone-name}"
  type    = "NS"
  ttl     = "30"
  records = [
    "${aws_route53_delegation_set.tdl-delegation-set.name_servers.0}",
    "${aws_route53_delegation_set.tdl-delegation-set.name_servers.1}",
    "${aws_route53_delegation_set.tdl-delegation-set.name_servers.2}",
    "${aws_route53_delegation_set.tdl-delegation-set.name_servers.3}",
  ]
}
