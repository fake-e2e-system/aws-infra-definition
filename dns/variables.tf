variable "region" {
  description = "Region that the instances will be created"
}

variable "tld-zone-name" {
  description = "the top level domain name"
}

variable "tld-zone-id" {
  description = "Route53 zone ID for the top level domain"
}

variable "dns-zones" {
  description = "a list of the zone names to create Route53 hosted zones for"
  type = "list"
}

variable "num-zones" {
  default = "The number of items in the dns-zone varible list"
}
