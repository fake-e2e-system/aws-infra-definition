variable "bucket_name" {
  description = "name of bucket"
}

variable "region" {
  description = "The region to launch the resources"
}
