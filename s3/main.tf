provider "aws" {
    region = "${var.region}"
    version = "~> 1.24"
}

/* S3 bucket to host the my static web content */

resource "aws_s3_bucket" "site" {
    bucket = "${var.bucket_name}"
    acl    = "public-read"
    website {
        index_document = "index.html"
        error_document = "404.html"
    }
    tags {
    }
    force_destroy = true
}

/* Upload the bucket object for the index page - move to build pipeline at some point */

resource "aws_s3_bucket_object" "index" {
  bucket = "${var.bucket_name}"
  acl    = "public-read"
  key    = "index.html"
  source = "html/index.html"
  content_type = "text/html"
  etag   = "${md5(file("html/index.html"))}"
  depends_on = [
  "aws_s3_bucket.site"
]
}

/* Upload the bucket object for the error page - move to build pipeline at some point */

resource "aws_s3_bucket_object" "error-404" {
  bucket = "${var.bucket_name}"
  acl    = "public-read"
  key    = "404.html"
  source = "html/404.html"
  content_type = "text/html"
  etag   = "${md5(file("html/404.html"))}"
  depends_on = [
  "aws_s3_bucket.site"
]
}

/* retrieve data from AWS on the dev.bigbluecow.net. hosted zone */

data "aws_route53_zone" "selected" {
  name         = "dev.bigbluecow.net."
  private_zone = false
}

/* create an A record for status.dev.bigbluecow.net and associate it with the s3 bucket*/

resource "aws_route53_record" "status" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "status.${data.aws_route53_zone.selected.name}"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.site.website_domain}"
    zone_id                = "${aws_s3_bucket.site.hosted_zone_id}"
    evaluate_target_health = true
  }
}
