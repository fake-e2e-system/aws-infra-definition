environment          = "dev-env"
region               = "eu-west-2"
availability_zone_a  = "eu-west-2a"
availability_zone_b  = "eu-west-2b"
key_name             = "dev-env"
instance_type        = "t2.nano"

# vpc
vpc_cidr_block        = "10.0.0.0/16"
public_subnet_a_cidr  = "10.0.1.0/24"
private_subnet_a_cidr = "10.0.2.0/24"
public_subnet_b_cidr  = "10.0.3.0/24"
private_subnet_b_cidr = "10.0.4.0/24"
