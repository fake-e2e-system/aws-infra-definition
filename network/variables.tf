variable "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
}

variable "instance_type" {
  description = "The instance type to launch"
}

variable "public_subnet_a_cidr" {
  description = "The CIDR block for the public subnet in AZ A"
}

variable "private_subnet_a_cidr" {
  description = "The CIDR block for the private subnet in AZ A"
}

variable "public_subnet_b_cidr" {
  description = "The CIDR block for the public subnet in AZ A"
}

variable "private_subnet_b_cidr" {
  description = "The CIDR block for the private subnet in AZ A"
}

variable "environment" {
  description = "The environment"
}

variable "region" {
  description = "The region to launch the resources"
}

variable "availability_zone_a" {
  description = "The first az that the resources will be launched"
}
variable "availability_zone_b" {
  description = "The second az that the resources will be launched"
}

variable "key_name" {
  description = "The public key for the bastion host"
}
