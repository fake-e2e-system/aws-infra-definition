provider "aws" {
  region = "${var.region}"
  version = "~> 1.24"
}

/* Load the public key for the enviroment in to the AWS EC2 service */

resource "aws_key_pair" "key" {
  key_name   = "${var.key_name}"
  public_key = "${file("${var.environment}_key.pub")}"
}

/* Ceate the VPC for the enviroment */

resource "aws_vpc" "vpc" {
  cidr_block           = "${var.vpc_cidr_block}"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags {
    Name        = "${var.environment}-vpc"
    Environment = "${var.environment}"
  }
}

/* Internet gateway for the public subnet */
resource "aws_internet_gateway" "ig" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.environment}-igw"
    Environment = "${var.environment}"
  }
}

/* Public Subnet in Avalability Zone A */
resource "aws_subnet" "subnet_a_1_public" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnet_a_cidr}"
  availability_zone       = "${var.availability_zone_a}"
  map_public_ip_on_launch = true

  tags {
    Name        = "${var.environment}-${var.availability_zone_a}-subnet1-public"
    Environment = "${var.environment}"
  }
}

/* Public Subnet in Avalability Zone B */
resource "aws_subnet" "subnet_b_3_public" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnet_b_cidr}"
  availability_zone       = "${var.availability_zone_b}"
  map_public_ip_on_launch = true

  tags {
    Name        = "${var.environment}-${var.availability_zone_b}-subnet3-public"
    Environment = "${var.environment}"
  }
}

/* Private subnet in Avalability Zone A */
resource "aws_subnet" "subnet_a_2_private" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnet_a_cidr}"
  availability_zone       = "${var.availability_zone_a}"
  map_public_ip_on_launch = false

  tags {
    Name        = "${var.environment}-${var.availability_zone_a}-subnet2-private"
    Environment = "${var.environment}"
  }
}

/* Private subnet in Avalability Zone B */
resource "aws_subnet" "subnet_b_4_private" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnet_b_cidr}"
  availability_zone       = "${var.availability_zone_b}"
  map_public_ip_on_launch = false

  tags {
    Name        = "${var.environment}-${var.availability_zone_b}-subnet4-private"
    Environment = "${var.environment}"
  }
}

/* Routing table for public subnet */
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.environment}-public-route-table"
    Environment = "${var.environment}"
  }
}

/* Route for the IGW in the public subnet */
resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.ig.id}"
}

/* Routing table for private subnet */
resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.environment}-private-route-table"
    Environment = "${var.environment}"
  }
}

/* Route table associations */
resource "aws_route_table_association" "public_a" {
  subnet_id      = "${aws_subnet.subnet_a_1_public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = "${aws_subnet.subnet_b_3_public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private_a" {
  subnet_id       = "${aws_subnet.subnet_a_2_private.id}"
  route_table_id  = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private_b" {
  subnet_id       = "${aws_subnet.subnet_b_4_private.id}"
  route_table_id  = "${aws_route_table.private.id}"
}

/* Default security group */
resource "aws_security_group" "default" {
  name        = "${var.environment}-default-sg"
  description = "Default security group to allow inbound/outbound from the VPC"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    self      = "true"
  }

  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    self      = "true"
  }

  tags {
    Name        = "${var.environment}-default-sg"
    Environment = "${var.environment}"
  }
}

/* Bastion Host security group */

resource "aws_security_group" "bastion" {
  vpc_id      = "${aws_vpc.vpc.id}"
  name        = "${var.environment}-bastion-host"
  description = "Allow SSH to bastion host"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
}
egress {
  from_port = 80
  to_port   = 80
  protocol  = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}
  tags {
    Name        = "${var.environment}-bastion-sg"
    Environment = "${var.environment}"
  }
}

/* Query the latest ubuntu AMI stored in the AWS account  */

data "aws_ami" "bastion_ami" {
  owners = ["self"]
  name_regex = "^bastion-server*"
  most_recent = true
}

/* Bastion host EC2 instance */

resource "aws_instance" "bastion" {
  ami                         = "${data.aws_ami.bastion_ami.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  monitoring                  = true
  vpc_security_group_ids      = ["${aws_security_group.bastion.id}"]
  subnet_id                   = "${aws_subnet.subnet_a_1_public.id}"
  associate_public_ip_address = true

  tags {
    Name        = "${var.environment}-bastion"
    Environment = "${var.environment}"
  }
}
