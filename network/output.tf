output "region" {
  value = "${var.region}"
}

output "environment" {
  value = "${var.environment}"
}

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "public_subnet_a_cidr" {
  value = "${var.public_subnet_a_cidr}"
}

output "public_subnet_b_cidr" {
  value = "${var.public_subnet_b_cidr}"
}

output "public_subnet_a_id" {
  value = "${aws_subnet.subnet_a_1_public.id}"
}

output "private_subnet_a_id" {
  value = "${aws_subnet.subnet_a_2_private.id}"
}

output "public_subnet_b_id" {
  value = "${aws_subnet.subnet_b_3_public.id}"
}

output "private_subnet_b_id" {
  value = "${aws_subnet.subnet_b_4_private.id}"
}

output "default_sg_id" {
  value = "${aws_security_group.default.id}"
}

output "bastion_public_ip" {
  value = "${aws_instance.bastion.public_ip}"
}
