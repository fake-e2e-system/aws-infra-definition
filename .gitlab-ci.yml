image: registry.gitlab.com/fake-e2e-system/docker-infra-tools

# Default output file for Terraform plan
variables:
  APP_PLAN: app_plan.tfplan
  DNS_PLAN: dns_plan.tfplan
  NET_PLAN: net_plan.tfplan
  S3_PLAN: s3_plan.tfplan

before_script:
  - aws --version
  - terraform --version
  - packer --version
  - ansible --version

stages:
  - validate
  - build_dns_net
  - deploy_dns_net
  - build_app_s3
  - deploy_app_s3
  - destroy_fail
  - destroy_teardown

validate_dev:
  stage: validate
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/dns/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-dns-lock" -backend-config "key=tf-dns/terraform.tfstate"
    - terraform workspace select default
    - terraform validate -var-file=terraform.tfvars
    - cd /builds/fake-e2e-system/aws-infra-definition/network/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-net-lock" -backend-config "key=tf-net/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform validate -var-file=dev-env.tfvars
    - cd /builds/fake-e2e-system/aws-infra-definition/application/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-app-lock" -backend-config "key=tf-app/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform validate -var-file=dev-env.tfvars
    - cd /builds/fake-e2e-system/aws-infra-definition/s3/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-s3-lock" -backend-config "key=tf-s3/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform validate -var-file=dev-env.tfvars

  environment:
    name: development
  only:
    - develop

build_dns:
  stage: build_dns_net
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/dns/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-dns-lock" -backend-config "key=tf-dns/terraform.tfstate"
    - terraform workspace select default
    - terraform plan -var-file=terraform.tfvars -out=$DNS_PLAN
  artifacts:
    name: plan
    paths:
       - /builds/fake-e2e-system/aws-infra-definition/dns/$DNS_PLAN
    expire_in: 1 hour
  environment:
    name: development
  when: on_success
  only:
    - develop

build_dev_net:
  stage: build_dns_net
  script:
   - cd /builds/fake-e2e-system/aws-infra-definition/network/
   - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-net-lock" -backend-config "key=tf-net/terraform.tfstate"
   - terraform workspace select dev-env
   - terraform plan -var-file=dev-env.tfvars -out=$NET_PLAN
  artifacts:
   name: plan
   paths:
   - /builds/fake-e2e-system/aws-infra-definition/network/$NET_PLAN
   expire_in: 1 hour
  environment:
    name: development
  when: on_success
  only:
   - develop

deploy_dns:
  stage: deploy_dns_net
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/dns/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-dns-lock" -backend-config "key=tf-dns/terraform.tfstate"
    - terraform workspace select default
    - terraform apply -input=false $DNS_PLAN
  environment:
    name: development
  dependencies:
    - build_dns
  when: on_success
  only:
    - develop

deploy_dev_net:
    stage: deploy_dns_net
    script:
      - cd /builds/fake-e2e-system/aws-infra-definition/network/
      - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-net-lock" -backend-config "key=tf-net/terraform.tfstate"
      - terraform workspace select dev-env
      - terraform apply -input=false $NET_PLAN
    environment:
      name: development
    dependencies:
      - build_dev_net
    when: on_success
    only:
      - develop

build_dev_app:
  stage: build_app_s3
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/application/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-app-lock" -backend-config "key=tf-app/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform plan -var-file=dev-env.tfvars -out=$APP_PLAN
  artifacts:
    name: plan
    paths:
       - /builds/fake-e2e-system/aws-infra-definition/application/$APP_PLAN
    expire_in: 1 hour
  environment:
    name: development
  when: on_success
  only:
    - develop

build_dev_s3:
 stage: build_app_s3
 script:
  - cd /builds/fake-e2e-system/aws-infra-definition/s3/
  - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-s3-lock" -backend-config "key=tf-s3/terraform.tfstate"
  - terraform workspace select dev-env
  - terraform plan -var-file=dev-env.tfvars -out=$S3_PLAN
 artifacts:
  name: plan
  paths:
   - /builds/fake-e2e-system/aws-infra-definition/s3/$S3_PLAN
  expire_in: 1 hour
 environment:
  name: development
 when: on_success
 only:
  - develop

deploy__dev_app:
  stage: deploy_app_s3
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/application/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-app-lock" -backend-config "key=tf-app/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform apply -input=false $APP_PLAN
  environment:
    name: development
  dependencies:
    - build_dev_app
  when: on_success
  only:
    - develop

deploy_dev_s3:
  stage: deploy_app_s3
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/s3/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-s3-lock" -backend-config "key=tf-s3/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform apply -input=false $S3_PLAN
  environment:
    name: development
  dependencies:
    - build_dev_s3
  when: on_success
  only:
    - develop

destroy_dev_teardown:
  stage: destroy_teardown
  script:
    - cd /builds/fake-e2e-system/aws-infra-definition/application/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-app-lock" -backend-config "key=tf-app/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform destroy -var-file=dev-env.tfvars -auto-approve
    - cd /builds/fake-e2e-system/aws-infra-definition/s3/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-s3-lock" -backend-config "key=tf-s3/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform destroy -var-file=dev-env.tfvars -auto-approve
    - cd /builds/fake-e2e-system/aws-infra-definition/network/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-net-lock" -backend-config "key=tf-net/terraform.tfstate"
    - terraform workspace select dev-env
    - terraform destroy -var-file=dev-env.tfvars -auto-approve
    - cd /builds/fake-e2e-system/aws-infra-definition/dns/
    - terraform init -backend-config "bucket=s3-remote-tfstate" -backend-config "dynamodb_table=tf-dns-lock" -backend-config "key=tf-dns/terraform.tfstate"
    - terraform workspace select default
    - terraform destroy -var-file=terraform.tfvars -auto-approve
  when: manual
  only:
    - develop
