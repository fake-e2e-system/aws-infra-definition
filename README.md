# Fake e2e system

Building an example end to end system on AWS using a simple microservice, Terraform, Packer, Ansible, Docker, GitLab CI, and other tools. With the purpose to better understand the creation of an automated application and infrastructure cycle and build.


## AWS Infrastructure Diagram - Development Environment

![Figure 1 - AWS Infrastructure Diagram](/images/fake_e2e_service.png)
