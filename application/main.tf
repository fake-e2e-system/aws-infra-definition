# Gather required data from networ remote state file and aws

provider "aws" {
  region = "${var.region}"
  version = "~> 1.24"
}

data "terraform_remote_state" "network" {
    backend = "s3"
    workspace = "${terraform.workspace}"
    config {
      bucket = "s3-remote-tfstate"
      key    = "tf-net/terraform.tfstate"
      region = "${var.region}"
    }
}

/* Security group for the app server instances */
resource "aws_security_group" "app_server_sg" {
  name        = "${data.terraform_remote_state.network.environment}-app-server-sg"
  description = "Security group for app that allows SSH & HTTP traffic from internet"
  vpc_id      = "${data.terraform_remote_state.network.vpc_id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["${data.terraform_remote_state.network.public_subnet_a_cidr}", "${data.terraform_remote_state.network.public_subnet_b_cidr}"]
  }

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    security_groups = ["${aws_security_group.lb_security_group.id}"]
  }

  tags {
    Name        = "${data.terraform_remote_state.network.environment}-app-server-sg"
    Environment = "${data.terraform_remote_state.network.environment}"
  }
}

/* Security group for the load balancer */

resource "aws_security_group" "lb_security_group" {
  name        = "${data.terraform_remote_state.network.environment}-lb-security-group"
  description = "Allow HTTP from Anywhere"
  vpc_id      = "${data.terraform_remote_state.network.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${data.terraform_remote_state.network.environment}-lb-inbound-sg"
  }
}

/* Find the ami name for the lastest Ubuntu AMI created by packer */

data "aws_ami" "app_ami" {
  owners = ["self"]
  name_regex = "^app-server*"
  most_recent = true
}


resource "aws_launch_configuration" "app-launchconfig" {
  name_prefix = "lc-app"
  security_groups = ["${aws_security_group.app_server_sg.id}"]
  image_id = "${data.aws_ami.app_ami.id}"
  instance_type = "${var.instance_type}"
  user_data = "${file("${path.module}/scripts/${data.terraform_remote_state.network.environment}_user_data.sh")}"
  key_name = "${var.key_name}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "app-autoscaling" {
  name = "app-autoscaling-${aws_launch_configuration.app-launchconfig.name}"
  vpc_zone_identifier = ["${data.terraform_remote_state.network.private_subnet_a_id}", "${data.terraform_remote_state.network.private_subnet_b_id}"]
  launch_configuration = "${aws_launch_configuration.app-launchconfig.name}"
  min_size = 2
  max_size = 2
  min_elb_capacity = 1
  health_check_grace_period = 300
  health_check_type = "ELB"
  termination_policies = ["OldestLaunchConfiguration"]
  load_balancers = ["${aws_elb.app_elb.name}"]
  lifecycle {
    create_before_destroy = true
  }

}

### Creating ELB
resource "aws_elb" "app_elb" {
  name = "${data.terraform_remote_state.network.environment}-app-elb"
  security_groups = ["${aws_security_group.lb_security_group.id}"]
  subnets = ["${data.terraform_remote_state.network.public_subnet_a_id}", "${data.terraform_remote_state.network.public_subnet_b_id}"]
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:8080/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "8080"
    instance_protocol = "http"
  }
}

/* Attach DNS to load balancer */

data "aws_route53_zone" "selected" {
  name         = "dev.bigbluecow.net."
  private_zone = false
}

resource "aws_route53_record" "fake" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "fake.${data.aws_route53_zone.selected.name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.app_elb.dns_name}"
    zone_id                = "${aws_elb.app_elb.zone_id}"
    evaluate_target_health = true
  }
}
