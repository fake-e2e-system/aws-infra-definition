output "elb_hostname" {
  value = "${aws_elb.app_elb.dns_name}"
}
