#!/bin/bash

#Run the nginx docker image on port 8080
docker run --restart=always -d --name docker-nginx -p 8080:80 registry.gitlab.com/fake-e2e-system/docker-nginx-hello:develop
