variable "region" {
  description = "Region that the instances will be created"
}

variable "key_name" {
  description = "The public key for the bastion host"
}

variable "instance_type" {
  description = "the ec2 instance type"
}
